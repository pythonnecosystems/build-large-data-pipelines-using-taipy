# Taipy를 이용한 대규모 데이터 파이프라인 구축

## 동기 
데이터를 효과적으로 관리하고 시각화하며 데이터 기반의 비즈니스 의사 결정을 내리기 위해서는 안정적이고 확장 가능하며 효율적이고 프로덕션에 바로 사용할 수 있는 데이터 파이프라인을 구축하는 것이 오늘날 많은 현대 비즈니스에서 필수적이다.

또한, 팀에서는 데이터 파이프라인을 원활한 상호 작용을 제공하는 본격적인 GUI 애플리케이션과 통합해야 하는 경우가 많다.

하지만 데이터가 증가함에 따라 기존 데이터 파이프라인에서 데이터를 효율적으로 처리하고 분석해야 하는 문제에 직면하는 경우가 많다.

![](./images/1_cSXsDDAL-Jg3VSgUL9zymw.webp)

- 런타임 증가
- 비효율적인 리소스 사용
- 확장 어려움
- 지연되는 인터페이스 등

일반적으로 이러한 문제는 효율적인 파이프라인 오케스트레이션 도구, 즉 함수와 파이프라인의 실행을 효과적으로 관리할 수 있는 도구가 부족하기 때문에 발생한다.

![](./images/1__84oz4mKfO6L_1PxLnRAcw.webp)

- 병렬 처리와 효율적인 리소스 할당을 통해 파이프라인 성능을 최적화한다.
- 여러 파이프라인과 그 종속성을 쉽게 관리할 수 있는 기능 제공
- 파이프라인 작업 실행의 올바른 순서 보장 등 다양한 기능을 제공한다.

이를 위해 타이피는 적은 코드로 안정적인 데이터 기반 파이프라인의 생성, 관리 및 실행을 간소화하는 오픈소스 도구이다.

따라서 이 포스팅에서는 Taipy를 활용하여 복잡한 대화형 데이터 파이프라인을 만드는 방법을 보여 줄 것이다.

시작한다 🚀!

## Taipy Core의 빌딩 블록
Taipy Core는 데이터 기반 어플리케이션을 보다 효율적이고 쉽게 구축할 수 있게 해주는 최신 백엔드 프레임워크이다.

파이프라인 개발을 진행하기 전에 이 프레임워크의 네 가지 주요 용어 데이터 노드, 작업, 파이프라인 및 시나리오를 이해하는 것이 중요하다.

### 1) 데이터 노드
이름에서 알 수 있듯이 데이터 노드는 데이터를 위한 플레이스홀더이다. 다음과 같은 모든 유형의 데이터를 참조할 수 있다.

![](./images/1_mo4rOXR0An7IR80NKf969Q.webp)

> *데이터 노드는 실제 데이터를 **보유하지는 않지만**, 데이터를 가져올 함수, 예상되는 데이터 타입 등 실제 데이터를 읽고 쓰는 데 필요한 모든 정보를 보유하고 있다.*

### 2) 작업
간단히 말해 작업은 파이프라인에서 특정 작업을 수행하도록 설계된 Python 함수이다.

![](./images/1_WmB88egaP_LPnPrqWLuawQ.webp)

Taipy Core에서 태스크는 데이터 노드를 입력으로 받아 데이터 노드로 표시되는 출력을 생성한다.

![](./images/1_HDXI6VVwe8w798k2kxRYGg.webp)

출력 데이터 노드는 파이프라인에서 다시 다양한 작업의 입력으로 사용될 수 있다.

또한 동일한 입력으로 이미 실행된 함수가 있다면 건너뛸 수도 있다. 기본적으로 동일한 입력으로 함수를 두 번 실행하면 동일한 출력을 얻을 수 있다(함수가 determistic한 경우).

함수를 두 번 실행하는 것은 리소스 낭비이다. Taipy에서는 건너뛰기 가능 속성을 사용하면 함수를 건너뛸 수 있다. [Skip task](https://bit.ly/47f5b6b)를 참고하세요.

마지막으로, 두 작업 사이에 종속성이 없는 경우 Taipy는 태스크를 병렬로 실행할 수 있으므로 어플리케이션의 런타임이 향상될 수 있다.

### 3) 파이프라인
파이프라인은 이름에서 알 수 있듯이 특정 목표를 달성하기 위해 고안된 일련의 작업이다.

![](./images/1_5hrdqLsNW1PNNfyW-lPF-g.webp)

작업과 마찬가지로 Taipy에서는 종속성이 없는 경우 파이프라인을 병렬로 실행할 수 있어 전체 데이터 파이프라인의 런타임을 크게 개선할 수 있다.

![](./images/1_Hf8Q36EPGlPf-r9DGVCDBQ.webp)

예를 들어, 위 그림에서 "clean data-source1" 및 "clean data-source2" 파이프라인을 병렬로 실행하여 성능을 개선할 수 있다.

### 4) 시나리오
마지막으로, 계층 구조에서 한 단계 더 올라가면 시나리오이다.

시나리오를 만들면 특정 목표를 향한 하나 이상의 파이프라인을 그룹화할 수 있다.

![](./images/1_vWF5RkIwr5M9pSaak9qdOA.webp)

## Taipy Core 시작하기
이제 Taipy Core의 내부 작동 원리를 알았으니 기본적인 데이터 파이프라인을 구축해 보자.

계속 진행하기 전에 몇 가지 패키지를 설치해야 한다.

### 1) Taipy 설치
먼저 다음과 같이 Taipy를 설치한다.

```bash
$ pip install taipy
```

### 2) Taipy 스튜디오 설치
Taipy는 데이터 노드, 작업, 파이프라인 및 시나리오를 정의할 수 있는 그래픽 편집기인 타이피 스튜디오라는 VS 코드 확장 프로그램을 제공한다.

주피터 노트북을 대신 사용하고자 하는 경우, [이 문서](https://bit.ly/47aIkIG)에서 자세한 내용을 확인하세요.

Taipy 스튜디오를 설치하려면 `VS Code` → `Extension` → `Search Taipy Studio` → `Install`를 수행한다.

![](./images/1_q3hx9wyU88H30GUFpPK5ow.webp)

설치가 완료되면 확장 프로그램이 VS Code의 활동 표시줄에 나타난다.

![](./images/1_ohaYLTlFByY86LYd-BSF0A.webp)

이제 설치가 끝났다!

## Taipy Core로 데이터 파이프라인 개발
다음으로 타이피 코어를 사용하여 간단한 파이프라인을 구축해 보겠다!

기본적으로 [Faker](https://pythonnecosystems.gitlab.io/testing/faker)를 사용하여 생성한 더미 직원 CSV 데이터를 로드하겠다. 처음 몇 행은 아래와 같다.

![](./images/1_JVWSTcbxX-gOva6JwcmWJA.webp)

직원 평균 평점이 가장 높은 회사를 찾는 작업이다.

이를 달성하기 위해 다음과 같이 수행한다.

- Taipy 스튜디오에서 데이터 노드(`input_data`)를 정의하고, 데이터 타입을 CSV로 지정하고, 데이터를 로드할 경로를 지정한다.
- `employee_data` 데이터 노드를 작업(`select_columns`)에 연결하여 필요한 두 개의 열, 즉 `Company_Name`과 `Employee_Rating`을 선택한다. 작업은 데이터 노드(`df_filtered`)를 출력한다.
`df_filtered`는 다른 작업(`highest_employee_rating`)의 입력으로 제공되며, 이 작업은 출력 데이터 노드(`top_company`)를 반환한다.

```
taip_core_demo
├── df_operations.py
├── config.toml
├── main.py
└── data
    └── dummy_data.csv
```

이 파일들을 하나씩 살펴보자.

### 1) `df_operations.py`: 작업용 스크립트
이 스크립트에서는 특정 작업에 해당하는 실행 코드를 정의한다.

예를 들어 `select_columns` 작업의 경우 다음과 같은 메서드가 있다:

```python
def select_columns(df):
    print("Task 1: Selecting Columns")

    columns = ["Company_Name", "Employee_Rating"]
    df_filtered = df[columns]

    return df_filtered
```

마찬가지로 highest_employee_rating 작업에 해당하는 코드도 이 파일에 정의되어 있다.

```python
def get_top_company(df):

    print("Task 2: Finding Top Company")
    
    grouped_rating = (df.groupby("Company_Name")
                        .Employee_Rating.mean()
                        .reset_index()
                      )

    top_employer = grouped_rating.nlargest(n=1, columns = ["Employee_Rating"])

    return top_employer
```

이 시점에서 Taipy Core가 특정 작업에 해당하는 특정 메서드를 어떻게 알 수 있는지 궁금할 수 있다.

이 정보는 TOML 파일에서 제공한다. 다음에서 이에 대해 설명할 것이다.

### 2) `config.toml`: 파이프라인 구성
다음과 같이 TOML 파일을 미리보기하여 보자.

- Taipy 스튜디오 확장 프로그램을 연다.
- 구성 파일을 마우스 오른쪽 버튼으로 클릭한다.
- `Taipy:Show View`를 선택한다.

![](./images/1_NjNvxvdIqk5eaecgW30vdQ.gif)

확장 프로그램을 열면 오른쪽 상단 모서리에 4개의 아이콘이 표시된다.

![](./images/1_YrqgjT64uRO_mUa4ausQcg.webp)

왼쪽부터 데이터 노드, 작업, 파이프라인과 시나리오를 나타낸다.

이제 데이터 파이프라인의 실행 순서를 정의하여 이 구성 파일을 채워 보겠다.

#### 1. 입력 데이터 노드 추가

![](./images/1_43zcf2FISp5jlgZ6WmCvoA.gif)

여기서는 데이터 노드를 추가하고, 저장소 유형을 CSV로 변경하고, `default_path` 속성을 지정한다.

파이프라인이나 시나리오를 만들 때 종종 데이터 노드 간에 데이터 노드를 공유하는 방법을 정의하고 싶을 수 있다.

즉, 중복을 피하기 위해 데이터 노드를 공유하고 싶을 수 있다. 또는 각 파이프라인에 로컬로 유지하고 싶을 수도 있다.

이는 Taipy에서 범위를 사용하여 정의할 수 있다. [Scope docs](https://bit.ly/3qjmO3Q)를 참고하세요.

#### 2. 데이터프레임 열 필터 작업을 추가하고 `select_columns()` 메서드에 연결한다.

다음으로 `select_columns` 작업을 정의하겠다.

![](./images/1_NkSkzZ4r5pnLb-JBvuX7xw.gif)

먼저 작업을 추가하고 이름을 `select_columns`으로 지정한다. 다음으로 입력 데이터 노드 `input_data`를 연결한다. 마지막으로 `df_operations.py`에 정의된 `select_columns()` 함수에 `select_columns` 작업을 첨부한다.

#### 3. `select_columns` 작업의 출력 데이터 노드 추가하기
`select_columns` 작업의 출력인 새 데이터 노드를 추가한다.

![](./images/1_hIsZA4gSfdyD02b1bHFpag.gif)

#### 4. `highest_employee_rating` 작업 추가하기
여기서는 이전 중간 데이터 노드를 새 작업에 연결한다. 또한 `df_operations.py` 파일에 정의된 해당 함수에 연결한다.

![](./images/1_AhPkeUiu2HMFr45O8mdApQ.gif)

#### 5. 출력 데이터 노드 추가
`highest_employee_rating` 작업의 출력을 다른 데이터 노드로 받는다.

![](./images/1_YzHg-MifGuopIX8BluPWfA.gif)

`config.toml` 파일이 거의 완성되었다. 두 단계만 더 거치면 된다.

#### 6. 파이프라인 만들기
`company_analysis` 파이프라인을 추가하고 이를 시작부터 끝까지 작업에 연결한다.

![](./images/1_8fXo49OU8u8Q9I-we1tRxA.gif)

#### 7. 시나리오 만들기
마지막으로 시나리오를 정의하고 이를 파이프라인에 연결한다.

![](./images/1_UC-L7mxaTNp9yiQePB10dg.gif)

이것으로 `config.toml` 파일을 완성했다.

### 3. `main.py`: 시나리오 로드와 실행
이는 데이터 파이프라인을 실행하기 위해 호출할 기본 스크립트이다.

```python
import taipy as tp # imports
from taipy.core.config import Config

Config.load("config.toml") # Load TOML file

my_scenario = Config.scenarios["Company_Analysis_Scenario"] # Load Scenario

if __name__ == "__main__":
    tp.Core().run() # Run Core
  
    # Create Scenario instance based on what was read from TOML file
    scenario = tp.create_scenario(my_scenario) 

    # Execute
    scenario.submit()

    # Fetch results
    print("Company with Highest Employee Rating: ", scenario.top_company.read())
```

임포트 후 TOML 구성 파일을 로드한다. 다음으로, `tp.Core().run()`을 사용해 Taipy Core를 시작한다.

다음 단계는 시나리오를 생성하고 모든 데이터 노드와 파이프라인에 정의된 작업을 `tp.create_scenario(my_scenario)`를 사용하여 인스턴스화하는 것이다.

시나리오를 실행하기 위해 `scenario.submit()`를 실행한다.

마지막으로, `scenario.top_company.read()`를 사용하여 원하는 데이터 노드의 출력을 검색한다.

![](./images/1_vfdgEPoxdOl23tNnXgR8_A.gif)

실행하면 위의 출력을 얻을 수 있다.

## Taipy 2.3의 최신 업데이트
이 포스팅을 쓰는 동안, Taipy는 데이터 파이프라인을 구축하기 위해 GUI 기능을 통합했다.

그 결과, Taipy는 이제 Taipy Core에서 정의한 엔티티에 원활하게 연결할 수 있는 즉시 사용 가능한 GUI 컨트롤 세트와 함께 제공된다.

위의 데모에서는 스크립트를 사용하여 시나리오를 생성하고 실행했다.

하지만 최신 업데이트를 통해 이제 GUI에서 직접 시나리오를 필터링하고 실행할 수도 있다.

즉, 이제 Taipy에는 Taipy에서 정의한 엔티티에 원활하게 연결되는 즉시 사용 가능한 GUI 컨트롤 세트가 제공된다.

그 결과, 어플리케이션은 엔티티를 시각화하고 GUI를 통해 엔티티와 상호 작용할 수 있다.

이 최신 업데이트를 통해 다음을 수행할 수 있다.

- 시나리오 생성
- 특정 시나리오 실행
- DAG 표시
- 데이터 노드를 선택하고 해당 데이터를 표시하는 등의 작업을 수행할 수 있다.

이러한 GUI 요소에 대한 자세한 정보는 [타이피 핵심 요소](https://bit.ly/3q9FRh0)에서 얻을 수 있다.

## 마치며
여기까지이다.

이번 포스팅에서는 Taipy Core로 엔드투엔드 데이터 파이프라인을 구축하는 방법을 설명하였다.

좀 더 구체적으로, 데이터 노드, 작업, 파이프라인, 시나리오 등 Taipy Core의 구성 요소를 다루었다.

그런 다음, 이 구성 요소를 사용하여 VS Code에서 Taipy Studio를 사용하여 데이터 파이프라인을 만들고 실행해 보았다.

이 포스팅에서는 Taipy를 사용하여 엔드투엔드 데이터 파이프라인을 구축하는 방법을 구체적으로 다루었지만, 타이피를 사용하여 low code의 강력한 GUI 애플리케이션을 구축할 수도 있다. [Taipy GUI](https://pythonnecosystems.gitlab.io/build-elegant-web-apps-for-data-projects)에서 이에 대한 시연해 볼 수 있을 것이다.
